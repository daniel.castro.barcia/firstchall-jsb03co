'use strict';

function calculator(op1, op2, typePo) {
  switch (typePo) {
    case 'suma':
      return op1 + op2;
    case 'resta':
      if (op2 > op1) {
        return op2 - op1;
      } else {
        return 'El segundo operador debe ser mayor que el primero';
      }
    case 'multiplica':
      return op1 * op2;
    case 'divide':
      if (op2 > 0) {
        return op1 / op2;
      } else {
        return 'No se puede realizar una división por 0';
      }
    case 'potencia':
      return op1 ** op2;
  }
}

console.log(calculator(2, 3, 'potencia'));

"use strict";

class Person {
  constructor(name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
  }

  propertiesPerson() {
    const property = [];
    property.push(this.name, this.age, this.gender);
    //console.log(property);
    return property;
  }
}

class Teacher extends Person {
  constructor(name, age, gender, subject) {
    super(name, age, gender);
    this.subject = subject;
    this.studendsCourses = [];
  }

  asignStudent(studentAdd) {
    // Comprueba si el nombre del estudiante ya existe en el array de este profesor

    let studentRepeat = false;
    for (let i = 0; i < this.studendsCourses.length; i++) {
      if (this.studendsCourses[i].name === studentAdd.name) {
        studentRepeat = true;
        //console.log("Sí está repetido: ", studentAdd.name);
      }
    }

    if (studentRepeat === false) {
      //console.log("No está repetido: ", studentAdd.name);
      this.studendsCourses.push(studentAdd);
    }
  }
}

class Student extends Person {
  constructor(name, age, gender) {
    super(name, age, gender);
    //this.studentData = [];
  }
  addStudent(course, group) {
    this.course = course;
    this.group = group;
  }
}

// Arrays con los datos
const teachers = ["Berto", "Iván", "David"];
const courses = ["Html5", "CSS", "JavaScript"];
const groups = ["Grupo1", "Grupo2", "Grupo3"];
const students = ["Jorge", "Ana", "Andrés", "Pablo", "Martín", "Nico"];
const ages = [34, 30, 40, 27, 35, 32];
const genders = [
  "Masculino",
  "Femenino",
  "Masculino",
  "Masculino",
  "Masculino",
  "Masculino"
];

function createPersons() {
  //Crea un persona de forma aleatoria a partir del array de estudiantes
  let numberStudent = 0;

  numberStudent = Math.floor(Math.random() * 6);

  const personAdd = new Person(
    students[numberStudent],
    ages[numberStudent],
    genders[numberStudent]
  ).propertiesPerson();

  return personAdd;
}

function createStudent(objStudent, course, group) {
  const studentAdd = new Student(objStudent[0], objStudent[1], objStudent[2]);
  studentAdd.addStudent(course, group);

  return studentAdd;
}
let nameTeacher = [];
let namePerson = [];
let nameStudent = [];

// Añade alumnos por cada profesor del Array
for (let n = 0; n < teachers.length; n++) {
  nameTeacher = new Teacher(teachers[n]);
  for (let j = 0; j < 6; j++) {
    //namePerson = createPersons();
    //  console.log(namePerson);
    nameStudent = createStudent(createPersons(), courses[0], groups[0]);
    nameTeacher.asignStudent(nameStudent);
  }
  console.log(
    "Alumnos del profesor ",
    nameTeacher.name,
    nameTeacher.studendsCourses
  );
}

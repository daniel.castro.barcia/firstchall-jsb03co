"use strict";

class Person {
  constructor(name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
  }

  propertiesPerson() {
    const property = [];
    property.push(this.name, this.age, this.gender);
    console.log(property);
  }
}

class Teacher extends Person {
  constructor(name, age, gender, subject) {
    super(name, age, gender);
    this.subject = subject;
  }

  asignStudent() {}
}

class Student extends Person {
  constructor(name, age, gender, course, group) {
    super(name, age, gender);
    this.course = course;
    this.group = group;
  }
  addStudent() {}
}

const teachers = ["Berto", "Iván", "David"];
const courses = ["Html5", "CSS", "JavaScript"];
const groups = ["Grupo1", "Grupo2", "Grupo3"];
const students = ["Jorge", "Ana", "Andrés", "Pablo", "Martín", "Nico"];
const ages = [34, 30, 40, 27, 35, 32];
const genders = [
  "Masculino",
  "Femenino",
  "Masculino",
  "Masculino",
  "Masculino",
  "Masculino"
];

function createPersons() {
  //Math.random()
  const random
  newPerson = new Person(students[i], ages[i], genders[i]).propertiesPerson();

  const studentCourse = new Student(newPerson);
}

//createPersons();

//const randomAnimal = Math.random() * 2 > 1 ? new Fox() : new Chicken();

//console.log(Math.random());


const numberGen = (Math.random()*10) < 6;
console.log(numberGen);
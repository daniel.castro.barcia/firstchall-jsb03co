"use strict";

let numberGen = 0;
let gameOver = 0;

while (gameOver < 50) {
  numberGen = Math.floor(Math.random() * 10);

  if (numberGen > 0 && numberGen < 7) {
    console.log("Numero generado: ", numberGen);
    gameOver += numberGen;
    console.log("Numero Acumulado: ", gameOver);
  }
  if (gameOver >= 50) {
    console.log("El juego ha terminado. Game Over");
  }
}
